package main

import "testing"

func TestLoadPosts(t *testing.T) {
	posts := loadPosts("posts/")
	if len(posts) == 0 {
		t.Errorf("No posts loaded")
	}
}
