Title: Got My Domain Back
Date: 10/20/2017

I let my domain lapse as I hadn't been using it for a while, but I finally got around to registering it again and have this site back up and running.

I've been playing around with WebGL, learning how to do that and how to generate geometry and textures algorithmically. It's been a lot of fun. I've also been familiarizing myself with some of the new features of ES6. Arrow functions are super handy!

So I'm hoping to post some of those experiments I can finish and polish up and post on here so I can actually show off some stuff. This page is pretty empty and I'd love to post more code and articles on stuff I'm working on. My day job keeps me pretty busy though but really, any self respecting web developer should have a halfway decent web site with some code!

Maybe I'll work on some of that stuff this weekend. If you're one of the 2 or 3 people who (maybe) actually visit this site, keep an eye out! Hopefully I will have some new stuff soon. I'll also post anything sigificant on my Twitter account, which is still the best way to get ahold of me if you are so inclined. You can find me at [@VenatioDecorus](https://twitter.com/venatiodecorus).
