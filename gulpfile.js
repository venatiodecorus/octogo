let gulp = require('gulp');
let sass = require('gulp-sass');
let prefix = require('gulp-autoprefixer');
let sourcemaps = require('gulp-sourcemaps');
let clean = require('gulp-clean-css');


gulp.task('sass', function() {
	gulp.src('static/css/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(prefix())
		.pipe(clean())
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest(function(f) {
			return f.base;
		}))
});

gulp.task('default', ['sass'], function() {
	gulp.watch('static/css/*.scss', ['sass']);
})