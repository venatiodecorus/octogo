FROM golang:latest
RUN mkdir /go/src/app
ADD . /go/src/app
WORKDIR /go/src/app
#RUN apt-get update && apt-get install -y libpcap0.8-dev entr
#RUN go get -u github.com/go-delve/delve/cmd/dlv
RUN go get
RUN go build -o octogo
#CMD dlv debug --headless --listen=:2345
CMD ./octogo
