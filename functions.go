package main

import (
	"bufio"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/golang-commonmark/markdown"
)

// Post - Struct to hold post information
type Post struct {
	Title      string
	Slug       string
	Date       time.Time
	DateString string
	Content    template.HTML
}

// Sort interface
type ByDate []Post

func (a ByDate) Len() int      { return len(a) }
func (a ByDate) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

// Using After instead of Before to get the decending sort that I want displayed
func (a ByDate) Less(i, j int) bool { return a[i].Date.After(a[j].Date) }

// Load posts from the provided path
func loadPosts(path string) []Post {
	// Get the list of files in our target directory
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Our return value
	var posts []Post

	// Markdown parser
	md := markdown.New(markdown.HTML(true))

	// Parse each file into a Post object
	for _, file := range files {
		//fmt.Println("Parsing ", file.Name())
		file, err := os.Open(path + file.Name())
		if err != nil {
			continue
		}
		defer file.Close()

		// Flag to check for metadata
		var meta = true
		// Post struct for this file
		var post Post
		// Slice to hold the content
		var lines []string
		// Parse this file and load it into post
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			//fmt.Println(line)
			// If we haven't exited the metadata block, check for the meta
			if meta {
				// Break on first blank line
				if len(line) == 0 {
					meta = false
					continue
				}
				split := strings.Split(line, ":")
				switch split[0] {
				case "Title":
					post.Title = split[1]
				case "Date":
					timeStr := strings.TrimSpace(split[1])
					post.Date, _ = time.Parse("01/02/2006", timeStr)
					post.DateString = post.Date.Format("2006-01-02")
				}
				continue
			}
			// Collect the rest of the content
			lines = append(lines, line)
		}
		// Dump the lines as a single string into our post object
		toParse := strings.Join(lines, "\n")
		post.Content = template.HTML(md.RenderToString([]byte(toParse)))
		post.Slug = post.Date.Format("2006-01-02") + strings.Join(strings.Split(strings.ToLower(post.Title), " "), "-")
		posts = append(posts, post)
	}
	sort.Sort(ByDate(posts))

	return posts
}

func getPostBySlug(posts []Post, slug string) Post {
	var ret Post
	for _, post := range posts {
		if post.Slug == slug {
			ret = post
		}
	}

	return ret
}
