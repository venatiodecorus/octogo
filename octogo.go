package main

import (
	"net/http"
	"path/filepath"

	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
)

// Configuration values
const (
	postsPath string = "./posts/"
)

//
// func createMyRender() multitemplate.Renderer {
// 	r := multitemplate.NewRenderer()
// 	r.AddFromFiles("index", "templates/base.tmpl", "templates/index2.tmpl")
// 	return r
// }

func loadTemplates(templatesDir string) multitemplate.Renderer {
	r := multitemplate.NewRenderer()

	layouts, _ := filepath.Glob(templatesDir + "/layouts/*.tmpl")
	includes, _ := filepath.Glob(templatesDir + "/includes/*.tmpl")

	for _, include := range includes {
		files := append(layouts, include)
		r.AddFromFiles(filepath.Base(include), files...)
	}

	return r
}

func main() {
	posts := loadPosts(postsPath)

	r := gin.Default()

	r.Static("/static", "./static")

	//r.LoadHTMLGlob("templates/*")
	r.HTMLRender = loadTemplates("./templates")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", nil)
	})

	r.GET("/blog", func(c *gin.Context) {
		c.HTML(http.StatusOK, "blog.tmpl", posts)
	})

	r.GET("/blog/:slug", func(c *gin.Context) {
		slug := c.Param("slug")
		c.HTML(http.StatusOK, "blogPost.tmpl", getPostBySlug(posts, slug))
	})

	r.GET("/contact", func(c *gin.Context) {
		c.HTML(http.StatusOK, "contact.tmpl", nil)
	})

	r.Run(":8080")

}
